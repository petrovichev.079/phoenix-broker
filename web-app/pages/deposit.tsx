import Head from 'next/head';
import { DepositPage } from '@/modules/transaction';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';

function Deposit() {
  return (
    <>
      <Head>
        <title>Пополнение счета | Phoenix Broker</title>
      </Head>
      <DepositPage />
    </>
  );
}

Deposit.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Deposit;
