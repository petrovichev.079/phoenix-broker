import Head from 'next/head';
import { RegisterPage } from '@/modules/auth';
import { Guest } from '@/components';
import { BaseLayout } from '@/layouts';

function Register() {
  return (
    <>
      <Head>
        <title>Регистрация | Phoenix Broker</title>
      </Head>
      <RegisterPage />
    </>
  );
}

Register.getLayout = (page) => (
  <Guest>
    <BaseLayout>{page}</BaseLayout>
  </Guest>
);

export default Register;
