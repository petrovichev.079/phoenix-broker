import Head from 'next/head';
import { ProfilePage } from '@/modules/profile';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';

function Profile() {
  return (
    <>
      <Head>
        <title>Мой профиль | Phoenix Broker</title>
      </Head>
      <ProfilePage />
    </>
  );
}

Profile.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Profile;
