import Head from 'next/head';
import { TradingPage } from '@/modules/trading';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';

function Trading() {
  return (
    <>
      <Head>
        <title>Торговля | Phoenix Broker</title>
      </Head>
      <TradingPage />
    </>
  );
}

Trading.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Trading;
