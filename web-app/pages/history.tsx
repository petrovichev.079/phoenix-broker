import Head from 'next/head';
import { HistoryPage } from '@/modules/history';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';

function History() {
  return (
    <>
      <Head>
        <title>История операций | Phoenix Broker</title>
      </Head>
      <HistoryPage />
    </>
  );
}

History.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default History;
