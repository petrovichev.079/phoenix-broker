import { Box, Typography, Container, Button } from '@mui/material';
import Head from 'next/head';
import type { ReactElement } from 'react';
import { BaseLayout } from '@/layouts';
import { Link } from '@/components';
import { CenterContent } from '@/components';

function Status404() {
  return (
    <>
      <Head>
        <title>Не найдено | Phoenix Broker</title>
      </Head>
      <CenterContent>
        <Box textAlign="center">
          <img alt="404" height={180} src="/static/images/404.svg" />
          <Typography variant="h2" sx={{ my: 2 }}>
            Что-то пошло не так!
          </Typography>
        </Box>
        <Container maxWidth="sm" sx={{ textAlign: 'center', mt: 3, p: 4 }}>
          <Button component={Link} href="/" variant="outlined">
            Перейти на главную
          </Button>
        </Container>
      </CenterContent>
    </>
  );
}

export default Status404;

Status404.getLayout = function getLayout(page: ReactElement) {
  return <BaseLayout>{page}</BaseLayout>;
};
