import Head from 'next/head';
import { WithdrawalPage } from '@/modules/transaction';
import { Authenticated } from '@/components';
import { MainLayout } from '@/layouts';

function Withdrawal() {
  return (
    <>
      <Head>
        <title>Вывод средств | Phoenix Broker</title>
      </Head>
      <WithdrawalPage />
    </>
  );
}

Withdrawal.getLayout = (page) => (
  <Authenticated>
    <MainLayout>{page}</MainLayout>
  </Authenticated>
);

export default Withdrawal;
