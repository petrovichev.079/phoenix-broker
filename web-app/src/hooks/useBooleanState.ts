import { useState } from 'react';

export function useBooleanState(initialState = false): [boolean, () => void] {
  const [isEdit, setEdit] = useState(initialState);
  const toggleEdit = () => setEdit(!isEdit);

  return [isEdit, toggleEdit];
}
