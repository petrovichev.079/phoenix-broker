import { useRouter } from 'next/router';

export function useRouterQueryId(param: string = 'id') {
  const { query } = useRouter();
  return parseInt(query[param] as string);
}
