import { ReactNode, createContext, useEffect, useReducer } from 'react';
import { client } from '@/api';
import { LoginRequest, LoginResponse, User } from '@/models';
import { getToken } from '@/utils';
import { RegisterRequest, RegisterResponse } from '@/models/register';

interface AuthState {
  isInitialized: boolean;
  isAuthenticated: boolean;
  user: User | null;
}

interface AuthContextValue extends AuthState {
  register: (data: RegisterRequest) => Promise<void>;
  login: (data: LoginRequest) => Promise<void>;
  logout: () => Promise<void>;
  invalidateUser: () => Promise<void>;
}

interface AuthProviderProps {
  children: ReactNode;
}

type InitializeAction = {
  type: 'INITIALIZE';
  payload: {
    isAuthenticated: boolean;
    user: User | null;
  };
};

type LoginAction = {
  type: 'LOGIN';
  payload: {
    user: User;
  };
};

type LogoutAction = {
  type: 'LOGOUT';
};

type UpdateAction = {
  type: 'UPDATE_USER';
  payload: {
    user: User;
  };
};

type Action = InitializeAction | LoginAction | LogoutAction | UpdateAction;

const initialAuthState: AuthState = {
  isAuthenticated: false,
  isInitialized: false,
  user: null
};

const handlers: Record<
  string,
  (state: AuthState, action: Action) => AuthState
> = {
  INITIALIZE: (state: AuthState, action: InitializeAction): AuthState => {
    const { isAuthenticated, user } = action.payload;

    return {
      ...state,
      isAuthenticated,
      isInitialized: true,
      user
    };
  },
  LOGIN: (state: AuthState, action: LoginAction): AuthState => {
    const { user } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user
    };
  },
  LOGOUT: (state: AuthState): AuthState => ({
    ...state,
    isAuthenticated: false,
    user: null
  }),
  UPDATE_USER: (state: AuthState, action: UpdateAction): AuthState => {
    const { user } = action.payload;

    return {
      ...state,
      user
    };
  }
};

const reducer = (state: AuthState, action: Action): AuthState =>
  handlers[action.type] ? handlers[action.type](state, action) : state;

export const AuthContext = createContext<AuthContextValue>({
  ...initialAuthState,
  register: () => Promise.resolve(),
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  invalidateUser: () => Promise.resolve()
});

export function AuthProvider({ children }: AuthProviderProps) {
  const [state, dispatch] = useReducer(reducer, initialAuthState);

  useEffect(() => {
    const initialize = async (): Promise<void> => {
      try {
        const authorization = getToken();

        const user = (
          await client.get('profile/user', {
            headers: { Authorization: authorization }
          })
        ).data;

        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: true,
            user: user.user
          }
        });
      } catch (err) {
        console.error(err);
        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: false,
            user: null
          }
        });
      }
    };

    initialize();
  }, []);

  const register = async (data: LoginRequest): Promise<void> => {
    const { token }: RegisterResponse = (
      await client.post('profile/register', data)
    ).data;

    if (token) {
      localStorage.setItem('accessToken', token);
      const user = (
        await client.get('profile/user', {
          headers: { Authorization: `Bearer ${token}` }
        })
      ).data;

      dispatch({
        type: 'LOGIN',
        payload: {
          user: user.user
        }
      });
    }
  };

  const login = async (data: LoginRequest): Promise<void> => {
    const { token }: LoginResponse = (await client.post('profile/login', data))
      .data;

    if (token) {
      localStorage.setItem('accessToken', token);
      const user = (
        await client.get('profile/user', {
          headers: { Authorization: `Bearer ${token}` }
        })
      ).data;

      dispatch({
        type: 'LOGIN',
        payload: {
          user: user.user
        }
      });
    }
  };

  const invalidateUser = async (): Promise<void> => {
    try {
      const user = (
        await client.get('profile/user', {
          headers: { Authorization: getToken() }
        })
      ).data;

      if (user) {
        dispatch({
          type: 'UPDATE_USER',
          payload: {
            user: user.user
          }
        });
      }
    } catch (err) {
      console.error(err);
    }
  };

  const logout = async (): Promise<void> => {
    localStorage.removeItem('accessToken');
    dispatch({ type: 'LOGOUT' });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        register,
        login,
        logout,
        invalidateUser
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export const AuthConsumer = AuthContext.Consumer;
