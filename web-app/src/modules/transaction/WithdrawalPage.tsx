import { useAuth } from '@/hooks';
import { Footer, StandartPageHeader } from '@/components';

export function WithdrawalPage() {
  const { user } = useAuth();

  return (
    <>
      <StandartPageHeader
        title="Вывод средств"
        caption="Может еще поторгуем?"
      />

      <Footer />
    </>
  );
}
