import { useAuth } from '@/hooks';
import { Footer, StandartPageHeader } from '@/components';

export function DepositPage() {
  const { user } = useAuth();

  return (
    <>
      <StandartPageHeader
        title="Пополнение счета"
        caption="Пришло время торговли!"
      />

      <Footer />
    </>
  );
}
