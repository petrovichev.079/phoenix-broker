import { Footer, StandartPageHeader } from '@/components';
import { useAuth } from '@/hooks';
import { Grid } from '@mui/material';
import { DashboardAccountBalance } from '@/modules/dashboard/DashboardAccountBalance';
import { DashboardAccounts } from '@/modules/dashboard/DashboardAccounts';

export function DashboardPage() {
  const { user } = useAuth();
  console.log(user);
  return (
    <>
      <StandartPageHeader
        title={`Добро пожаловать, ${user?.name}!`}
        caption="Сегодня хороший день, чтобы начать торговать на валютном рынке!"
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item xs={12}>
          <DashboardAccountBalance />
        </Grid>
        <Grid item xs={12}>
          <DashboardAccounts />
        </Grid>
      </Grid>

      <Footer />
    </>
  );
}
