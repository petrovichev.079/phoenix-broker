import { Button, CircularProgress, TextField } from '@mui/material';
import { useAuth, useRefMounted } from '@/hooks';
import { useRouter } from 'next/router';
import { useFormik } from 'formik';
import * as Yup from 'yup';

export function RegisterForm() {
  const { register } = useAuth();
  const isMountedRef = useRefMounted();
  const router = useRouter();

  const formik = useFormik({
    initialValues: {
      name: '',
      surname: '',
      email: '',
      password: '',
      submit: null
    },
    validationSchema: Yup.object({
      name: Yup.string().required('Не введено имя.'),
      surname: Yup.string().required('Не введена фамилия.'),
      email: Yup.string()
        .email('Введен некорректный Email.')
        .max(255)
        .required('Не введен Email.'),
      password: Yup.string()
        .min(8, 'Должно быть не меньше 8 символов.')
        .max(255)
        .required('Не введен пароль.'),
      submit: null
    }),
    onSubmit: async (values, helpers): Promise<void> => {
      try {
        await register(values);

        if (isMountedRef()) {
          const backTo = (router.query.backTo as string) || '/dashboard';
          router.push(backTo);
        }
      } catch (err) {
        console.error(err);
        if (isMountedRef()) {
          helpers.setStatus({ success: false });
          helpers.setErrors({
            submit: 'Некорректная регистрация.'
          });
          helpers.setSubmitting(false);
        }
      }
    }
  });

  return (
    <form noValidate onSubmit={formik.handleSubmit}>
      <TextField
        error={Boolean(formik.touched.name && formik.errors.name)}
        helperText={formik.touched.name && formik.errors.name}
        fullWidth
        margin="normal"
        label="Имя"
        name="name"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="text"
        value={formik.values.name}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.surname && formik.errors.surname)}
        helperText={formik.touched.surname && formik.errors.surname}
        fullWidth
        margin="normal"
        label="Фамилия"
        name="surname"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="text"
        value={formik.values.surname}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.email && formik.errors.email)}
        helperText={formik.touched.email && formik.errors.email}
        fullWidth
        margin="normal"
        label="Email"
        name="email"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="email"
        value={formik.values.email}
        variant="outlined"
      />
      <TextField
        error={Boolean(formik.touched.password && formik.errors.password)}
        helperText={formik.touched.password && formik.errors.password}
        fullWidth
        margin="normal"
        label="Пароль"
        name="password"
        onBlur={formik.handleBlur}
        onChange={formik.handleChange}
        type="password"
        value={formik.values.password}
        variant="outlined"
      />

      <Button
        sx={{
          mt: 3
        }}
        color="primary"
        startIcon={
          formik.isSubmitting ? <CircularProgress size="1rem" /> : null
        }
        disabled={formik.isSubmitting}
        type="submit"
        fullWidth
        size="large"
        variant="contained"
      >
        Зарегистрироваться
      </Button>
    </form>
  );
}
