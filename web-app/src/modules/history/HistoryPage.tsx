import { Footer, StandartPageHeader } from '@/components';
import { HistoryList } from '@/modules/history/HistoryList';

export function HistoryPage() {
  return (
    <>
      <StandartPageHeader
        title="История операций"
        caption="Здесь Вы можете посмотреть историю операций."
      />

      <HistoryList />

      <Footer />
    </>
  );
}
