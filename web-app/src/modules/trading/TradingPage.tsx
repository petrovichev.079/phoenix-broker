import { Footer, StandartPageHeader } from '@/components';
import {
  Card,
  CardContent,
  Grid,
  Button,
  Menu,
  CardHeader,
  MenuItem,
  Divider
} from '@mui/material';
import { TechAnalysisChart } from '@/components/TechAnalysisChart';
import { useRef, useState } from 'react';
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import { TradingExchange } from '@/modules/trading/TradingExchange';

export function TradingPage() {
  const actionRef1 = useRef<any>(null);
  const [openPeriod, setOpenMenuPeriod] = useState<boolean>(false);
  const [currency, setCurrency] = useState<string>('USDRUB');

  const currencies = ['USDRUB', 'EURRUB', 'GBPRUB'];

  return (
    <>
      <StandartPageHeader
        title={'Торговля'}
        caption="Сегодня хороший день, чтобы начать торговать на валютном рынке!"
      />

      <Grid
        sx={{ px: 4 }}
        container
        direction="row"
        justifyContent="center"
        alignItems="stretch"
        spacing={3}
      >
        <Grid item lg={8} md={8} xs={12}>
          <TechAnalysisChart symbol={currency} />
        </Grid>
        <Grid item lg={4} md={4} xs={12}>
          <Card sx={{ height: '500px !important' }}>
            <CardHeader
              action={
                <>
                  <Button
                    size="small"
                    variant="outlined"
                    ref={actionRef1}
                    onClick={() => setOpenMenuPeriod(true)}
                    endIcon={<ExpandMoreTwoToneIcon fontSize="small" />}
                  >
                    {currency}
                  </Button>
                  <Menu
                    disableScrollLock
                    anchorEl={actionRef1.current}
                    onClose={() => setOpenMenuPeriod(false)}
                    open={openPeriod}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'right'
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'right'
                    }}
                  >
                    {currencies.map((currency) => (
                      <MenuItem
                        key={currency}
                        onClick={() => {
                          setCurrency(currency);
                          setOpenMenuPeriod(false);
                        }}
                      >
                        {currency}
                      </MenuItem>
                    ))}
                  </Menu>
                </>
              }
              title="Валютные пары"
            />
            <Divider />
            <CardContent>
              <TradingExchange />
            </CardContent>
          </Card>
        </Grid>
      </Grid>

      <Footer />
    </>
  );
}
