import axios from 'axios';
import { getToken } from '@/utils';

// const { NEXT_PUBLIC_API_BASE_URL } = process.env;

export const client = axios.create({
  baseURL: `https://phoenix-broker-api.herokuapp.com/api/`,
  headers: { Authorization: getToken() }
});
