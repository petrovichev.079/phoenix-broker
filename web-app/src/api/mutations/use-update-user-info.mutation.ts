import { useMutation, UseMutationOptions } from 'react-query';
import { client } from '@/api';

export interface UpdateUserInfoRequest {
  id: number;
  name: string;
  surname: string;
  phone?: string;
  avatarPath?: string;
}

export function useUpdateUserInfoMutation(
  options: Pick<
    UseMutationOptions<unknown, unknown, UpdateUserInfoRequest>,
    'onError' | 'onSuccess'
  >
) {
  const mutation = useMutation<unknown, unknown, UpdateUserInfoRequest>(
    (data) => client.post('profile/user/update', data),
    options
  );

  return mutation.mutateAsync;
}
