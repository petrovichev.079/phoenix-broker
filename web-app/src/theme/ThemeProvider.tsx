import { FC } from 'react';
import { ThemeProvider } from '@mui/material';
import { StylesProvider } from '@mui/styles';
import { MyTheme } from '@/theme/theme';

const ThemeProviderWrapper: FC = (props) => {
  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={MyTheme}>{props.children}</ThemeProvider>
    </StylesProvider>
  );
};

export default ThemeProviderWrapper;
