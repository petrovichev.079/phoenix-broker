import { UserRoles } from '@/models/enums';

export interface User {
  id: number;
  email: string;
  role: UserRoles;
  name: string;
  surname: string;
  phone: string;
  avatarPath: string;
  verified: boolean;
  blocked: boolean;
}
