import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  DialogProps
} from '@mui/material';
import { ReactNode } from 'react';

export type DialogPopupProps = Omit<DialogProps, 'children'> & {
  children: ReactNode;
  header: ReactNode;
  actions: ReactNode;
};

export function DialogPopup({
  children,
  header,
  actions,
  ...forwardingProps
}: DialogPopupProps) {
  return (
    <Dialog fullWidth maxWidth="sm" {...forwardingProps}>
      <DialogTitle
        sx={{
          p: 3
        }}
      >
        {header}
      </DialogTitle>
      <DialogContent
        dividers
        sx={{
          p: 3
        }}
      >
        {children}
      </DialogContent>
      <DialogActions
        sx={{
          p: 3
        }}
      >
        {actions}
      </DialogActions>
    </Dialog>
  );
}
