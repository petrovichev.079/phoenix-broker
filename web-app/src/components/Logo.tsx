import Image from 'next/image';

interface LogoProps {
  size?: number;
}

export function Logo({ size = 40 }: LogoProps) {
  return (
    <Image
      alt="Феникс"
      height={size}
      width={size}
      src="/static/images/logo.svg"
    />
  );
}
