import { useCallback, useEffect } from 'react';
import Script from 'next/script';
import { getRandomInRange } from '@/utils';
import { Card, CardContent } from '@mui/material';

declare global {
  const TradingView: {
    widget: (options: any) => void;
  };
}

interface TechAnalysisChartProps {
  symbol: string;
}

export function TechAnalysisChart({ symbol }: TechAnalysisChartProps) {
  const container_id = `tech-analysis-chart-${symbol}-${getRandomInRange(
    10000000,
    99999999
  )}`;

  const init = useCallback(() => {
    new TradingView.widget({
      height: '100%',
      width: '100%',
      symbol: `FX_IDC:${symbol}`,
      interval: 'D',
      timezone: 'Europe/Moscow',
      theme: 'light',
      style: '1',
      locale: 'ru',
      hide_side_toolbar: false,
      enable_publishing: false,
      container_id
    });
  }, [symbol]);

  useEffect(() => {
    if (typeof TradingView !== 'undefined') {
      init();
    }
  }, [symbol]);

  return (
    <>
      <Card>
        <CardContent sx={{ height: '500px !important' }}>
          <div id={container_id} />
        </CardContent>
      </Card>

      <Script src="https://s3.tradingview.com/tv.js" onLoad={init} />
    </>
  );
}
