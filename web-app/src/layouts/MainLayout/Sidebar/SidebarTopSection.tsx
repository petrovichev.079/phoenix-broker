import { Box, Stack, Typography, useTheme } from '@mui/material';
import { Logo } from '@/components';

export function SidebarTopSection() {
  const theme = useTheme();

  return (
    <Box mt={3}>
      <Stack mx={4} direction="row" alignItems="center" spacing={2}>
        <Logo />
        <Typography
          variant="h4"
          sx={{
            color: `${theme.colors.alpha.trueWhite[100]}`
          }}
        >
          Phoenix Broker
        </Typography>
      </Stack>
    </Box>
  );
}
