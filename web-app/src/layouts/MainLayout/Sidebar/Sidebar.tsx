import { useContext } from 'react';
import { Scrollbar } from '@/components';
import { SidebarContext } from '@/contexts';
import {
  Box,
  Drawer,
  alpha,
  styled,
  Divider,
  useTheme,
  lighten,
  darken
} from '@mui/material';
import { SidebarTopSection } from '@/layouts/MainLayout/Sidebar/SidebarTopSection';
import { SidebarFooter } from '@/layouts/MainLayout/Sidebar/SidebarFooter';
import { SidebarUser } from '@/layouts/MainLayout/Sidebar/SidebarUser';
import { SidebarMenu } from '@/layouts/MainLayout/Sidebar/SidebarMenu';

export function Sidebar() {
  const { sidebarToggle, toggleSidebar } = useContext(SidebarContext);
  const closeSidebar = () => toggleSidebar();
  const theme = useTheme();

  return (
    <>
      <SidebarWrapper
        sx={{
          display: {
            xs: 'none',
            lg: 'inline-block'
          },
          position: 'fixed',
          left: 0,
          top: 0,
          background:
            theme.palette.mode === 'dark'
              ? alpha(lighten(theme.header.background, 0.1), 0.5)
              : darken(theme.colors.alpha.black[100], 0.5),
          boxShadow:
            theme.palette.mode === 'dark' ? theme.sidebar.boxShadow : 'none'
        }}
      >
        <Scrollbar>
          <SidebarTopSection />
          <SidebarDivider
            sx={{
              my: theme.spacing(3),
              mx: theme.spacing(2)
            }}
          />
          <SidebarUser />
          <SidebarDivider
            sx={{
              my: theme.spacing(3),
              mx: theme.spacing(2)
            }}
          />
          <SidebarMenu />
        </Scrollbar>
        <SidebarDivider />
        <SidebarFooter />
      </SidebarWrapper>
      <Drawer
        sx={{
          boxShadow: `${theme.sidebar.boxShadow}`
        }}
        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
        open={sidebarToggle}
        onClose={closeSidebar}
        variant="temporary"
        elevation={9}
      >
        <SidebarWrapper
          sx={{
            background:
              theme.palette.mode === 'dark'
                ? theme.colors.alpha.white[100]
                : darken(theme.colors.alpha.black[100], 0.5)
          }}
        >
          <Scrollbar>
            <SidebarTopSection />
            <SidebarDivider
              sx={{
                my: theme.spacing(3),
                mx: theme.spacing(2)
              }}
            />
            <SidebarUser />
            <SidebarDivider
              sx={{
                my: theme.spacing(3),
                mx: theme.spacing(2)
              }}
            />
            <SidebarMenu />
          </Scrollbar>
          <SidebarFooter />
        </SidebarWrapper>
      </Drawer>
    </>
  );
}

const SidebarWrapper = styled(Box)(
  ({ theme }) => `
        width: ${theme.sidebar.width};
        min-width: ${theme.sidebar.width};
        color: ${theme.colors.alpha.trueWhite[70]};
        position: relative;
        z-index: 5;
        height: 100%;
        padding-bottom: 61px;
`
);

const SidebarDivider = styled(Divider)(
  ({ theme }) => `
        background: ${theme.colors.alpha.trueWhite[10]};
`
);
