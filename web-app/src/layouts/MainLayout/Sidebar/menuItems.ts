import type { ReactNode } from 'react';
import CurrencyExchangeIcon from '@mui/icons-material/CurrencyExchange';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import InfoIcon from '@mui/icons-material/Info';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import DonutSmallIcon from '@mui/icons-material/DonutSmall';
import HistoryToggleOffIcon from '@mui/icons-material/HistoryToggleOff';
import EjectIcon from '@mui/icons-material/Eject';

export interface MenuItem {
  link: string;
  icon: ReactNode;
  name: string;
}

export interface MenuItems {
  items: MenuItem[];
  heading: string;
}

export const menuItems: MenuItems[] = [
  {
    heading: 'Меню',
    items: [
      {
        name: 'Аналитика',
        link: '/dashboard',
        icon: DonutSmallIcon
      },
      {
        name: 'Торговля',
        link: '/trading',
        icon: CurrencyExchangeIcon
      }
    ]
  },
  {
    heading: 'Движение денег',
    items: [
      {
        name: 'Пополнение счета',
        link: '/deposit',
        icon: AddCircleOutlineIcon
      },
      {
        name: 'Вывод средств',
        link: '/withdrawal',
        icon: EjectIcon
      },
      {
        name: 'История операций',
        link: '/history',
        icon: HistoryToggleOffIcon
      }
    ]
  },
  {
    heading: 'Другое',
    items: [
      {
        name: 'Мой профиль',
        link: '/profile',
        icon: AccountBoxIcon
      },
      {
        name: 'О проекте',
        link: '/',
        icon: InfoIcon
      }
    ]
  }
];
