import { Avatar, Box, Typography, useTheme } from '@mui/material';
import { useAuth } from '@/hooks';

export function SidebarUser() {
  const theme = useTheme();
  const { user } = useAuth();

  return (
    <Box
      sx={{
        textAlign: 'center',
        mx: 2,
        pt: 1,
        position: 'relative'
      }}
    >
      <Avatar
        sx={{
          width: 68,
          height: 68,
          mb: 2,
          mx: 'auto'
        }}
        alt={user?.name}
        src={user?.avatarPath}
      />

      <Typography
        variant="h4"
        sx={{
          color: `${theme.colors.alpha.trueWhite[100]}`
        }}
      >
        {user?.name} {user?.surname}
      </Typography>
      <Typography
        variant="subtitle1"
        sx={{
          color: `${theme.colors.alpha.trueWhite[70]}`
        }}
      >
        {user?.email}
      </Typography>
    </Box>
  );
}
