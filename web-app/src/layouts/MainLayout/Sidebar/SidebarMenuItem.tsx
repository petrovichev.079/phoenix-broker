import { useContext } from 'react';
import clsx from 'clsx';
import { SidebarContext } from '@/contexts';
import { Button, ListItem } from '@mui/material';
import { Link } from '@/components';

interface SidebarMenuItemProps {
  link: string;
  icon: any;
  name: string;
  active: boolean;
}

export function SidebarMenuItem({
  link,
  icon: Icon,
  active,
  name,
  ...rest
}: SidebarMenuItemProps) {
  const { closeSidebar } = useContext(SidebarContext);

  return (
    <ListItem component="div" {...rest}>
      <Button
        disableRipple
        component={Link}
        href={link}
        className={clsx({ 'Mui-active': active })}
        onClick={closeSidebar}
        startIcon={Icon && <Icon />}
      >
        {name}
      </Button>
    </ListItem>
  );
}
