# Phoenix Broker

Участникам предстоит спроектировать и реализовать приложение для проведения операций на торговых площадках, в частности, на валютном рынке. Ожидаемый результат — полноценное работающее web/mobile-решение, в котором реализован обозначенный функционал.

## Стек технологий

PHP, Laravel; TypeScript, React, NextJS, MaterialUI; Docker, Gitlab CI/CD.

## Демо

WEB: https://phoenix-broker.herokuapp.com/

Доступы: 
- test@test.ru
- 12345678

## Развёртывание

Сервисы запускаются внутри докер контейнеров.
Команды нужно выпонять в корневой директории проекта.

API
```
cd infrastructure
docker-compose up --build
```

WEB
```
docker build -f web-app/Dockerfile -t ssnet_web .
docker run -p 8081:8081 -e "NEXTAUTH_URL='localhost:8081'" -e "PORT=8081" -it ssnet_web
```
