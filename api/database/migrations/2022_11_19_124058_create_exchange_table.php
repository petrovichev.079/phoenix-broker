<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExchangeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchange', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fromAccountId')
                ->constrained('account')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('toAccountId')
                ->constrained('account')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->float('amountGiven');
            $table->float('amountReceived');
            $table->float('rate');
            $table->foreignId('currencyPairId')
                ->constrained('currencyPair')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchange');
    }
}
