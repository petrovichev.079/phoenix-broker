<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname', 255)
                ->nullable();
            $table->string('phone', 30)
                ->nullable();
            $table->string('avatarPath', 255)
                ->nullable();
            $table->boolean('verified')
                ->default(false);
            $table->enum('role', ['admin', 'customer'])
                ->default('customer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('phone');
            $table->dropColumn('avatarPath');
            $table->dropColumn('verified');
            $table->dropColumn('role');
        });
    }
}
