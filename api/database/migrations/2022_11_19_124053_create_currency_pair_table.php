<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrencyPairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencyPair', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->foreignId('firstCurrencyId')
                ->constrained('currency')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreignId('secondCurrencyId')
                ->constrained('currency')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencyPair');
    }
}
