<?php

use GuzzleHttp\Client;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Currency\Currency;
use App\Models\Currency as CurrencyModel;
use App\Models\CurrencyPair;

class FillCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $client = new Client([
            'base_uri' => 'https://api.apilayer.com/',
        ]);

        $currencies = (new Currency($client))->getCurrencies();
        $codes = [];
        $currencyCodeId = [];
        foreach ($currencies as $code => $name) {
            $currency = CurrencyModel::create([
                'name' => $name,
                'code' => $code
            ]);
            $currencyCodeId[$code] = $currency->id;
            $codes[] = $code;
        }

        for ($i = 0; $i < count($codes); $i++) {
            for ($j = 0; $j < count($codes); $j++) {
                if ($codes[$i] === $codes[$j]) {
                    continue;
                }

                CurrencyPair::create([
                    'name' => "{$codes[$i]}{$codes[$j]}",
                    'firstCurrencyId' => $currencyCodeId[$codes[$i]],
                    'secondCurrencyId' => $currencyCodeId[$codes[$j]],
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        CurrencyModel::truncate();
        CurrencyPair::truncate();
    }
}
