<?php

namespace App\Http\Controllers\Api;

use App\Enums\CurrencyEnum;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class Profile extends Controller
{
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $user = (new User())->where('email', $request->email)->first();

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        if (! Hash::check($request->password, $user->password)) {
            return response()->json(['error' => 'Password is not equal'], 404);
        }

        session()->push('user_email', $user->email);

        return response()->json(['token' => $user->remember_token]);
    }

    public function user(Request $request): JsonResponse
    {
        $token = $request->bearerToken();

        $user = User::where(['remember_token' => $token])->first();

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        return response()->json(['user' => $user->toArray(), 'account' => $user->getAccount('RUB')]);
    }

    public function account(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => 'required|integer',
            'currencyCode' => 'required',
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        return response()->json($user->getAccount($request->currencyCode));
    }

    public function accounts(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => 'required|integer'
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        return response()->json($user->getAccounts());
    }

    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users'],
            'surname' => ['required', 'string'],
            'password' => ['required', Password::min(8)],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'surname' => $request->surname,
            'password' => Hash::make($request->password),
        ]);

        $token = $user->createToken($request->email)->plainTextToken;

        (new User)->where('id', $user->id)->update(['remember_token' => $token]);

        $user->createAccount(CurrencyEnum::RUB);

        return response()->json(['token' => $token]);
    }

    public function isVerified(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => ['required', 'integer'],
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        return response()->json(['isVerified' => $user->verifies ?? false]);
    }

    public function verify(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => ['required', 'integer'],
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        $user->update(['verified' => true]);

        return response()->json($user->toArray());
    }

    public function blocked(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => ['required', 'integer'],
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        $user->update(['blocked' => true]);

        return response()->json($user->toArray());
    }

    public function unblock(Request $request)
    {
        $request->validate([
            'userId' => ['required', 'integer'],
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        $user->update(['blocked' => false]);

        return response()->json($user->toArray());
    }

    public function isBlocked(Request $request): JsonResponse
    {
        $request->validate([
            'userId' => ['required', 'integer'],
        ]);

        $user = User::find($request->userId);

        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        return response()->json(['blocked' => $user->blocked]);
    }

    public function update(Request $request)
    {
        $request->validate([
            'id' => ['required', 'integer'],
            'name' => ['required'],
            'surname' => ['required'],
        ]);

        $update = [
            'name' => $request->name,
            'surname' => $request->surname,
        ];

        $user = User::find($request->id);
        if (! $user) {
            return response()->json(['error' => 'Person not found'], 404);
        }

        if (isset($request->phone)) {
            $update['phone'] = $request->phone;
        }

        if (isset($request->avatarPath)) {
            $update['avatarPath'] = $request->avatarPath;
        }

        $user->update($update);

        return response()->json($user->toArray());
    }
}
