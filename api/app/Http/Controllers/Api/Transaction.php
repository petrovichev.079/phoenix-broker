<?php

namespace App\Http\Controllers\Api;

use App\Currency\CurrencyConverter;
use App\Http\Controllers\Controller;
use App\Models\Account;
use App\Models\CurrencyPair;
use App\Models\Exchange;
use App\Models\Transaction as TransactionModel;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Currency;

class Transaction extends Controller
{
    public function deposit(Request $request): JsonResponse
    {
        $request->validate([
            'accountId' => 'required|integer',
            'amount' => 'required',
        ]);

        $account = Account::find($request->accountId);

        if (! $account) {
            return response()->json(['error' => 'Account not found'], 404);
        }

        $transaction = TransactionModel::create([
            'accountId' => $account->id,
            'amount' => $request->amount,
            'type' => 'deposit'
        ]);

        $account->deposit($request->amount);

        return response()->json([
            'account' => $account->toArray(),
            'transaction' => $transaction->toArray(),
        ]);
    }

    public function withdrawal(Request $request): JsonResponse
    {
        $request->validate([
            'accountId' => 'required|integer',
            'amount' => 'required',
        ]);

        $account = Account::find($request->accountId);

        if ($account->value - $request->amount < 0) {
            return response()->json(['error' => 'No money'], 400);
        }

        if (! $account) {
            return response()->json(['error' => 'Account not found'], 404);
        }

        $transaction = TransactionModel::create([
            'accountId' => $account->id,
            'amount' => $request->amount,
            'type' => 'withdrawal'
        ]);

        $account->withdrawal($request->amount);

        return response()->json([
            'account' => $account->toArray(),
            'transaction' => $transaction->toArray(),
        ]);
    }

    public function list(Request $request): JsonResponse
    {
        $request->validate([
            'accountId' => 'required|integer'
        ]);

        $account = Account::find($request->accountId);
        if (! $account) {
            return response()->json(['error' => 'Account not found'], 404);
        }

        $transactions = (new TransactionModel())->where('accountId', $request->accountId)->get();
        $exchange = (new Exchange())->where('fromAcceountId', $request->accountId)
            ->where('toAccountId', $request->accountId)->get();

        return response()->json([
            'transactions' => $transactions->toArray(),
            'exchange' => $exchange->toArray(),
        ]);
    }



    public function exchange(Request $request): JsonResponse
    {
        $request->validate([
            'fromAccountId' => ['required', 'integer'],
            'toAccountId' => ['required', 'integer'],
            'amount' => ['required'],
        ]);

        $fromAccount = Account::find($request->fromAccountId);
        $toAccount = Account::find($request->toAccountId);

        if (! $toAccount || ! $fromAccount) {
            return response()->json(['error' => 'Person not found'], 400);
        }

        $currencyFrom = Currency::find($fromAccount->currencyId)->code;
        $currencyTo = Currency::find($toAccount->currencyId)->code;

        $fromAccount->withdrawal($request->amount);
        $toAccount->deposit($request->amount);

        $client = new Client([
            'base_uri' => 'https://api.apilayer.com/',
        ]);

        $currencyConverter = (new CurrencyConverter($client));

        $amountReceived = $currencyConverter
            ->from($currencyFrom)
            ->to($currencyTo)
            ->amount($request->amount)
            ->convert();

        $rate = $currencyConverter
            ->from($currencyFrom)
            ->to($currencyTo)
            ->amount(1)
            ->convert();

        $currencyPair = CurrencyPair::where('name', "{$currencyFrom}{$currencyTo}")->first();

        $exchange = Exchange::create([
            'fromAccountId' => $fromAccount->id,
            'toAccountId' => $toAccount->id,
            'amountGiven' => $request->amount,
            'amountReceived' => $amountReceived,
            'currencyPairId' => $currencyPair->id,
            'rate' => $rate,
        ]);

        return response()->json($exchange->toArray());
    }
}
