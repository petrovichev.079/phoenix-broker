<?php

namespace App\Http\Controllers\Api;

use App\Currency\CurrencyConverter;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class Currency extends Controller
{
    public function convert(Request $request)
    {
        $request->validate([
            'fromCurrencyCode' => ['required'],
            'toCurrencyCode' => ['required'],
            'amount' => ['required']
        ]);

        $client = new Client([
            'base_uri' => 'https://api.apilayer.com/',
        ]);

        $currencyConverter = (new CurrencyConverter($client));

        $converted = $currencyConverter
            ->from($request->fromCurrencyCode)
            ->to($request->toCurrencyCode)
            ->amount($request->amount)
            ->convert();

        $rate = $currencyConverter
            ->from($request->fromCurrencyCode)
            ->to($request->toCurrencyCode)
            ->amount(1)
            ->convert();

        return response()->json(['result' => $converted, 'rate' => $rate]);
    }
}
