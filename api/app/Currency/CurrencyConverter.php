<?php

namespace App\Currency;

use App\Currency\Common\BaseCurrencyConverter;
use GuzzleHttp\Client;

class CurrencyConverter extends BaseCurrencyConverter
{
    protected $apiToken = 'XBNGrQSpAgSwNQhGrRVnS3j7kmUlSczM';

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function convert(): float
    {
        $response = $this->client->request('GET', 'currency_data/convert', [
            'headers' => [
                'apikey' => $this->apiToken,
            ],
            'query' => [
                'to' => $this->to,
                'from' => $this->from,
                'amount' => $this->amount
            ]
        ]);

        $body = json_decode($response->getBody());

        return $body->result;
    }
}
