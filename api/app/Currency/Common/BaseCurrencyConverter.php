<?php

namespace App\Currency\Common;

abstract class BaseCurrencyConverter
{
    protected $to;

    protected $from;

    protected $amount;

    protected $client;

    public function to(string $to): BaseCurrencyConverter
    {
        $this->to = $to;

        return $this;
    }

    public function from(string $from): BaseCurrencyConverter
    {
        $this->from = $from;

        return $this;
    }

    public function amount(int $amount): BaseCurrencyConverter
    {
        $this->amount = $amount;

        return $this;
    }

    public abstract function convert(): float;
}
