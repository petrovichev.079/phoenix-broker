<?php

namespace App\Currency;

use GuzzleHttp\Client;

class Currency
{
    protected $apiToken = 'XBNGrQSpAgSwNQhGrRVnS3j7kmUlSczM';

    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getCurrencies()
    {
        $response = $this->client->request('GET', 'currency_data/list', [
            'headers' => [
                'apikey' => $this->apiToken,
            ],
        ]);

        return json_decode($response->getBody())->currencies;
    }
}
