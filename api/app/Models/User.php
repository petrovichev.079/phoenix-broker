<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'verified',
        'blocked',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function createAccount(string|array $currencyCode)
    {
        if (! is_array($currencyCode)) {
            $currencyCode = [$currencyCode];
        }

        $currencies = Currency::whereIn('code', $currencyCode)->get('id');

        foreach ($currencies as $currency) {
            Account::create([
                'userId' => $this->id,
                'currencyId' => $currency->id,
                'value' => 0,
                'number' => Str::uuid(),
            ]);
        }
    }

    public function getAccounts()
    {
        return Account::where('userId', $this->id)->get()->toArray();
    }

    public function getAccount(string $currencyCode)
    {
        return Account::where('currencyId', (new Currency())->getCurrencyByCode($currencyCode)->id)
            ->where('userId', $this->id)
            ->get()
            ->toArray();
    }
}
