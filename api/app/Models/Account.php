<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'account';

    protected $fillable = [
        'userId',
        'currencyId',
        'value',
        'number',
    ];

    public function deposit(float $amount)
    {
        $value = $this->value + $amount;
        return $this->update(['value' => $value]);
    }

    public function withdrawal(float $amount)
    {
        $value = $this->value - $amount;
        return $this->update(['value' => $value]);
    }


}
