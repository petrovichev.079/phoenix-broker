<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';

    protected $fillable = [
        'code',
        'name',
    ];

    /**
     * @param string $code
     * @return static|null
     */
    public function getCurrencyByCode(string $code): ?static
    {
        return $this->where('code', $code)->first() ?? null;
    }
}
