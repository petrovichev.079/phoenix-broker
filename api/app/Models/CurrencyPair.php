<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyPair extends Model
{
    protected $table = 'currencyPair';

    protected $fillable = [
        'name',
        'firstCurrencyId',
        'secondCurrencyId',
    ];
}
