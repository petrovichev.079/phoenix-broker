<?php

use App\Http\Controllers\Api\Auth;
use App\Http\Controllers\Api\Currency;
use App\Http\Controllers\Api\Profile;
use App\Http\Controllers\Api\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/token', [Auth::class, 'token']);

Route::post('profile/register', [Profile::class, 'create']);

Route::post('profile/login', [Profile::class, 'login']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('profile/user', [Profile::class, 'user']);

    Route::get('profile/user/update', [Profile::class, 'update']);

    Route::get('profile/accounts', [Profile::class, 'accounts']);

    Route::get('profile/account', [Profile::class, 'account']);

    Route::get('profile/isVerified', [Profile::class, 'isVerified']);

    Route::post('profile/verify', [Profile::class, 'verify']);

    Route::post('profile/blocked', [Profile::class, 'blocked']);

    Route::post('profile/isBlocked', [Profile::class, 'isBlocked']);


    Route::post('transaction/deposit', [Transaction::class, 'deposit']);

    Route::post('transaction/withdrawal', [Transaction::class, 'withdrawal']);

    Route::post('transaction/list', [Transaction::class, 'list']);

    Route::post('transaction/exchange', [Transaction::class, 'exchange']);


    Route::get('currency/convert', [Currency::class, 'convert']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
